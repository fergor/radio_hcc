---
bibliography: 'bibliography.bib'
csl: 'GOST\_R\_7011.csl'

---

# Радиомика в определении генетического типа гепатоцеллюлярного рака.

## Введение

[@lambinRadiomicsExtractingMore2012]

## Список литературы

{#список-литературы .unnumbered}

